import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'src/app/services/common.service';
import { ReactiveFormsModule, FormControl, FormGroup, Validators } from '@angular/forms';

export interface IDialogData {
  title: string;
  content: string[];
  subContent: string[];
  submitButton: boolean;
  Type:string;
}

interface IinputType {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  public dialogRef: MatDialogRef<DialogComponent>;
  public data: IDialogData;
  myForm: FormGroup;

  inputTypes: IinputType[] = [
    { value: 'text', viewValue: 'text' },
    { value: 'password', viewValue: 'password' },
    { value: 'email', viewValue: 'email' },
    { value: 'number', viewValue: 'number' }
  ];

  buttonTypes: IinputType[] = [
    {value: 'Submit', viewValue: 'Submit'},
    {value: 'Reset', viewValue: 'Reset'}
  ];

  checkboxTypes: IinputType[] = [
    {value: 'Female', viewValue: 'Female'},
    {value: 'Male', viewValue: 'Male'}
  ];

  public constructor(dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) data: IDialogData, private commonService :CommonService) {
    try {
      this.dialogRef = dialogRef;
      this.data = data;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
  ngOnInit(){
    if(this.data.Type == "Input"){
      this.myForm = new FormGroup({          
        'inputType':new FormControl(null),
        'value':new FormControl(null,[Validators.required])
    })
  }
  if(this.data.Type == "Button"){
    this.myForm = new FormGroup({          
      'buttonType':new FormControl(null),
  })
  }
  if(this.data.Type == "CheckBox"){
    this.myForm = new FormGroup({          
      'checkBoxType':new FormControl(null),
  })
}
 this.myForm.valueChanges.subscribe(res =>{
   if(res && res.inputType){
    this.myForm.controls.value.setValidators([Validators.required])
    switch(res.inputType){
      case "email":
        this.myForm.controls.value.setValidators([Validators.required, Validators.email])
        break;
        case "number":
          this.myForm.controls.value.setValidators([Validators.required, Validators.pattern('[0-9]+')])
        break;
    }
   }
 })
  }

  public save(event): void {
    try {
      if(this.myForm.valid){
        this.commonService.dialogData$.next(this.myForm.value);
        this.dialogRef.close();
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  public close(): void {
    try {
      this.dialogRef.close();
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

}
