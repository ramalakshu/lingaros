import { Component, OnDestroy } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IDialogData, DialogComponent } from '../app/dialog/dialog/dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { CommonService } from './services/common.service';

interface IWork {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  private readonly formBuilder: FormBuilder = new FormBuilder();
  public workSpaceCtrl: FormGroup = this.formBuilder.group({});

  leftPanelButtonsList;

  inputvalue: string = '';

  leftPanelShow: boolean = true;
  jsonStructurShow: boolean = true;
  publishButtonShow: boolean = true;
  DownloadButtonShow: boolean = false;
  Button = "Button"
  checkboxlabel = "Checkbox label"
  jsondata = [];
  workspace = [];

  leftPanelButtons: string[] = [
    'Input Box',
    'Select Box',
    'Check Box',
    'Radio Button',
    'Button'
  ];

  works: IWork[] = [
    { value: 'Task-0', viewValue: 'Task' },
    { value: 'UserStory-1', viewValue: 'UserStory' },
    { value: 'Bug-2', viewValue: 'Bug' }
  ];

  constructor(public dialog: MatDialog, private commonService: CommonService) {
    this.commonService.dialogData$.pipe(untilDestroyed(this)).subscribe(response => {
      console.log(response);
      this.jsondata.forEach((element,index) => {
        if(element && element.Name == "input" && response['inputType'] != null){
          this.inputvalue = response['value'];
          this.jsondata[index].Value = response['value'];
          this.jsondata[index].Type = response['inputType'];
        }
        if(element && element.Name == "Button" && response['buttonType'] != null){
           this.Button = response['buttonType'];
           this.jsondata[index].Type = response['buttonType'];
        }
        if(element && element.Name == "checkbox" && response['checkBoxType'] != null){
          this.checkboxlabel = response['checkBoxType'];
          this.jsondata[index].Name = response['checkBoxType'];
       }
      });
    });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);

      this.workSpaceCtrl.addControl(this.workspace[event.previousIndex], this.formBuilder.control(''));

      const ButtonType = this.workspace[event.currentIndex];

      let payload;

      switch (ButtonType) {
        case 'Input Box':
          payload = {
            Name: 'input',
            Type: 'text',
            Value: 'textfield'
          };
          break;

        case 'Select Box':
          payload = {
            Name: 'select',
          };
          break;

        case 'Check Box':
          payload = {
            Name: 'checkbox',
          };
          break;
          case 'Radio Button':
          payload = {
            Name: 'RadioButton',
          };
          break;

        case 'Button':
          payload = {
            Name: 'Button',
            Type: 'submit',
            Value: 'Done'
          };
          break;
      }

      this.jsondata.push(payload);
    }
  }

  inputClicked(event) {
    if(!this.DownloadButtonShow)
    this.openDialog("Input");
  }
  checkBoxClicked(event) {
    if(!this.DownloadButtonShow)
    this.openDialog("CheckBox");
  }
  buttonClicked(event) {
    if(!this.DownloadButtonShow)
    this.openDialog("Button");
  }

  public openDialog(type): void {
    try {
      const dialogData: IDialogData = {
        title: 'Set Property for the input type',
        content: [],
        subContent: [],
        submitButton: true,
        Type:type
      };

      const dialogRef: MatDialogRef<DialogComponent> = this.dialog.open(DialogComponent, {
        width: '500px',
        data: dialogData,
        disableClose: true
      });

      dialogRef.afterClosed()
        .pipe(untilDestroyed(this))
        .subscribe((result): void => {
          console.log(result);
        });
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  publishIconClick(event) {
    try {
      this.leftPanelShow = false;
      this.jsonStructurShow = false;
      this.publishButtonShow = false;
      this.DownloadButtonShow = true;
    } catch (error) {
      console.error(error);
      throw error;
    }
  }

  ngOnDestroy() { }

}
